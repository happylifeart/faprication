
const youtube_channels = {
    /* A - Channels that start with the letter A */
    /* B - Channels that start with the letter B */
    /* C - Channels that start with the letter C */
    /* D - Channels that start with the letter D */
    /* E - Channels that start with the letter E */
    /* F - Channels that start with the letter F */
    /* G - Channels that start with the letter G */
    /* H - Channels that start with the letter H */
    /* I - Channels that start with the letter I */
    /* J - Channels that start with the letter J */
    /* K - Channels that start with the letter K */
    /* L - Channels that start with the letter L */
    /* M - Channels that start with the letter M */
    /* N - Channels that start with the letter N */
    /* O - Channels that start with the letter O */
    /* P - Channels that start with the letter P */
    /* Q - Channels that start with the letter Q */
    /* R - Channels that start with the letter R */
    /* S - Channels that start with the letter S */
    /* T - Channels that start with the letter T */
    /* U - Channels that start with the letter U */
    /* V - Channels that start with the letter V */
    /* W - Channels that start with the letter W */
    /* X - Channels that start with the letter X */
    /* Y - Channels that start with the letter Y */
    /* Z - Channels that start with the letter Z */
}

export default youtube_channels